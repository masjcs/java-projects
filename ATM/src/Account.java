
public class Account {
	private String name;
	private int pin;
	private double savings;
	private double checking;
	
	public Account(String name, int pin, double savings, double checking){
		setName(name);
		setPin(pin);
		setSavings(savings);
		setChecking(checking);
		
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setPin(int pin){
		this.pin = pin;
	}
	
	public void setSavings(double savings){
		this.savings = savings;
	}
	
	public void setChecking(double checking){
		this.checking = checking;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getPin(){
		return this.pin;
	}
	
	public double getSavings(){
		return this.savings;
	}
	
	public double getChecking(){
		return this.checking;
	}
	

}
