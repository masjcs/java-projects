

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.BoxLayout;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Main  {
	static Account a = new Account("Jake Johnson", 1432, 5000, 500);
	//*Declaration*//
	private static JButton button2;
	private static JButton button1;
	private static JButton button3;
	private static JButton button4;
	private static JButton button5;
	private static JButton button6;
	private static JLabel label1;
	private static JFrame frame1;
	private static JFrame frame2;
	private static JPanel panel1;
	private static JPanel panel2;
	private static JTextField tf1;
	
	private static DecimalFormat twoDForm;
	
	private static double withdraw;
	
	public static void main(String[] args){
		//*Instantiation*//
		button1			= new JButton("Withdraw");
		button2			= new JButton("Deposit");
		button3			= new JButton("Balance");
		button4			= new JButton("Transfer");
		button5			= new JButton("OK");
		button6			= new JButton("Cancel");
		label1			= new JLabel("Select an Option");
		frame1			= new JFrame("Account Information");
		frame2			= new JFrame("Withdrawal");
		panel1			= new JPanel();
		panel2			= new JPanel();
		tf1				= new JTextField(15);
		twoDForm 		= new DecimalFormat("#.##");
		//*Location*//
		
		panel1.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 20));
		panel1.add(label1);
		button1.addActionListener(
		         new ActionListener() {
		            public void actionPerformed( ActionEvent e )
		            {
		            	tf1.setLayout(new FlowLayout(FlowLayout.CENTER));
		            	tf1.setText("Enter Withdrawal Amount");
		            	panel2.add(tf1);
		            	panel2.add(button5);
		            	panel2.add(button6);
		            	frame2.add(panel2);
		            	frame2.pack();
		            	frame2.setVisible(true);
		            }
		         }
		      );
		button5.addActionListener(
		         new ActionListener() {
		            public void actionPerformed( ActionEvent e )
		            {
		            	try{
		            		
		            		withdraw			= Double.parseDouble(tf1.getText());
		            		withdraw	= Double.valueOf(twoDForm.format(withdraw));
		            		System.out.println(withdraw);
		            		
		            	}catch(Exception a){
		            		
		            		System.out.println("Not a Double");
		            	}
		            }
		         }
		      );
		button6.addActionListener(
		         new ActionListener() {
		            public void actionPerformed( ActionEvent e )
		            {
		            	frame2.dispose();
		            }
		         }
		      );
		panel1.add(button1);
		panel1.add(button2);
		panel1.add(button3);
		panel1.add(button4);
		
		frame1.add(panel1);
		
		//*Other*//
		
		frame1.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );   
		frame1.pack();
		frame1.setVisible(true);
		
		

		
		
    
	}

	
	
}
