
public class Magic {

	public static String[] magicTypes = {"Quick-Play", "Field", "Equip", "Ritual", "Normal", "Continuous"};
	
	private String type;
	
	Magic(String type){
		setType(type);
	}
	
	
	public void magicEffect(){
		//put revive(),discard() etc. methods here
		//Inherit those methods
	}
	//setters
	public void setType(String type){
		this.type = type;
	}
	
	//getters
	public String getType(){
		return this.type;
	}
}
