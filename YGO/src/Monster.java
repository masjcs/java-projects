
public class Monster {
	private int attack;
	private int defense;
	private String type;
	private String attribute;
	private String name;
	private boolean effect;
	public static final String[] monsterType = {"Aqua", "Beast", "Beast-Warrior", "Dinosaur", "Dragon", "Fairy", "Fiend", 
			"Fish", "Insect", "Machine", "Plant", "Psychic", "Pyro", "Reptile", "Rock", "Sea Serpent", "Spellcaster", 
			"Thunder", "Warrior", "Winged-Beast", "Zombie"};
	public static final String[] monsterAttribute = {"Dark", "Divine", "Earth", "Fire", "Light", "Water", "Wind"};
	
	Monster(String name, int attack, int defense, String type, String attribute, boolean effect){
		//add checks in? type can't be normal and effect can't be true
		setName(name);
		setAttack(attack);
		setDefense(defense);
		setType(type);
		setAttribute(attribute);
		setEffect(effect);
		
	}
	
	public void monsterEffect(){
		//do monster effects here revive(),discard(), etc.
		//Inherit this method?
		//Leave Empty
	}
	
	public void setEffect(boolean effect){
		this.effect= effect;
	}
	
	public void setDefense(int defense){
		this.defense= defense;
	}
	
	public void setType(String type){
		this.type= type;
	}
	
	public void setAttribute(String attribute){
		this.attribute = attribute;
	}
	
	public void setAttack(int attack){
		this.attack= attack;	
	}
	
	public void setName(String name){
		this.name= name;
	}
	
	//getters
	public boolean getEffect(){
		return this.effect;
	}
	
	public int getAttack(){
		return this.attack;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getDefense(){
		return this.defense;
	}
	
	public String getType(){
		return this.type;
	}
	
	public String getAttribute(){
		return this.attribute;
	}
	
}
